class forFairhurst{
	public static void  main(String[] args){
		String s="";
		for(int i=0;i<args.length;i++){ //concatenate all the arguments into one string
			s=s+args[i]+" ";
		}
		for(int i=0;i<s.length();i++){
			System.out.println(s); //Print out all shifted results
			s=moveChars(s); //Shift the characters by 1
		}
	}
	
	public static String moveChars(String s){
		String ret="";
		for(int i=0;i<s.length()-1;i++){
			ret=ret+s.charAt(i+1);//Bring all but the last character to the left
		}
		ret=ret+s.charAt(0); //replace the last unchanged character to the original’s first
		return ret;
	}
}
